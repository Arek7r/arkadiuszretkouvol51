﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerSystem : MonoBehaviour
{


    private GameObject bullet;


    //Do ustawienia w unity
    public float minPowerBullet = 100f;
    public float maxPowerBullet = 150f;

    private int i = 0; //licznik ruchu, max 12

    void OnEnable()
    {
        GameManager.ResetBehavior += Reset;
    }

    void OnDisable()
    {
        GameManager.ResetBehavior -= Reset;
    }

    void Start()
    {
        bullet = transform.Find("Bullet").gameObject; //wiem nie optymalne
        bullet.SetActive(false);
        StartCoroutine(RotateTower());
    }



    IEnumerator RotateTower()
    {
        while (i <= 12)
        {
            yield return new WaitForSeconds(2f);
            transform.rotation *= Quaternion.AngleAxis(Random.Range(15f, 45f), Vector3.up);
            FireBullet();
            i++;
            if (i >= 12)
            {
                gameObject.GetComponent<Renderer>().material.color = Color.red;
                //StopCoroutine(RotateTower());
            }
        }
    }


    //Wiem miało być 4 jednostki
    //moj pomysl jest taki by losowac odleglosc 1-4jednostek
    //potem jakims wzorem policzyc sile kuli by upadla na "tej odleglosci"
    // gdyby byl target to posluzylbym sie lookAt i dopiero strzelal
    private void FireBullet()
    {
        GameObject bulletClone = Instantiate(bullet, bullet.transform.position, bullet.transform.rotation) as GameObject;
        bulletClone.SetActive(true);
        Rigidbody rb = bulletClone.GetComponent<Rigidbody>();

        float power = (Random.Range(minPowerBullet, maxPowerBullet));
        rb.AddForce(bullet.transform.forward * power);
    }



    private void Reset()
    {
        i = 0;
        gameObject.GetComponent<Renderer>().material.color = Color.white;
       // StartCoroutine(RotateTower());
    }
}
