﻿using UnityEngine;
using UnityEngine.UI;

// Nie napisalem poola choc powinienen byc, bo zawsze korzystalem z gotowego i nie bede poswiecal teraz czasu by sie "nauczyc" go pisac. Ale grunt ze jestem swiadom pewnych rzeczy. 


public class GameManager : MonoBehaviour
{
    public static int towersCounter = 1;
    public static GameObject tower;
    //public static GameObject bullet;    //Chcialem zrobic static ale i tak trzeba pobierac miejsce gdzie ma byc spawn kuli (do Instantiate w sc: TowerSystem) wiec moim zdaniem wydajnosciowo wychodzi na to samo; Albo dodac metode z Instantiate do obiektu gdzie ma sie spawnic - ale to juz jest komplikowanie

    public GameObject towerPrefab;
    public GameObject canvasText;
    public static Text text;

    public delegate void ResetTowers();
    public static event ResetTowers ResetBehavior; // cialo w towersystem



    void Start()
    {
        text = canvasText.GetComponent<Text>();
        //tower = GameObject.FindGameObjectWithTag("TowerPrime");
        //by Tag bo szybciej niz zwykle find
        //by dzialalo poprawnie trzeba ustawic pierwsza "wzorowa" wieze tam gdzie nie zostanie zniszczona bo jesli zostanie zniszczona to caly misterny plan...

        tower = towerPrefab;
    }


    public static void ResetMethod()
    {
        ResetBehavior();
    }

    public static void AddValueCounter()
    {
        towersCounter++;
        text.text = "Towers: " + towersCounter;
    }

    public static void SubValueCounter()
    {
        towersCounter--;
        text.text = "Towers: " + towersCounter;
    }

}
