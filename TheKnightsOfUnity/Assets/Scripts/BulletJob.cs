﻿using UnityEngine;


public class BulletJob : MonoBehaviour
{


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("TowerPrime"))
        {
            Destroy(this.gameObject);
            Destroy(collision.gameObject);
            GameManager.SubValueCounter();
        }

        else
        {
            Destroy(this.gameObject);

            if (GameManager.towersCounter <= 100)
            {
                //ToDo: //(Vector3.up * 1 / 2) <-hard code
                GameObject towerClone = Instantiate(GameManager.tower,
                    transform.position + (Vector3.up * 1 / 2),
                    GameManager.tower.transform.rotation) as GameObject;
                GameManager.AddValueCounter();
            }
            else
            {
                GameManager.ResetMethod(); //jesli jest juz 100 wiez wolamy metode w ktorej jest "wyzwalacz resetu"
            }

        }

    }
}

